const candidato01 = {
    nome: "Rafael",
    nota: 8.75
};
const candidato02 = {
    nome: "Thiago",
    nota: 8.
};
const candidato03 = {
    nome: "Umberto",
    nota: 10
};
const candidato04 = {
    nome: "Bruna",
    nota: 9.75
};
const candidato05 = {
    nome: "Josefa",
    nota: 9
};

let candidatos = []

candidatos.push(candidato01);
candidatos.push(candidato02);
candidatos.push(candidato03);
candidatos.push(candidato04);
candidatos.push(candidato05);

//console.log(candidatos);

function compare(a,b){
    if (a.nota < b.nota){
        return -1;
    }
    if (a.nota > b.nota){
        return 1;
    }
    return 0;
}

candidatos.sort(compare);

const numeroDeCandidatos = candidatos.length;


console.log("Primeiro colocado do Vestibular "+(candidatos[numeroDeCandidatos-1]).nome);
console.log("Segundo colocado do Vestibular "+(candidatos[numeroDeCandidatos-2]).nome);
console.log("Terceiro colocado do Vestibular "+(candidatos[numeroDeCandidatos-3]).nome);
