import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { CreateFilmeDto } from './dto/create-filme.dto';
import { UpdateFilmeDto } from './dto/update-filme.dto';
import { Filme, FilmeDocument } from './entities/filme.entity';
import { Model } from 'mongoose';

@Injectable()
export class FilmesService {

  constructor(@InjectModel(Filme.name) private filmeModel: Model<FilmeDocument>) {}

  create(createFilmeDto: CreateFilmeDto) {
    const filme = new this.filmeModel(createFilmeDto);
    return filme.save();
  }

  findAll() {
    return this.filmeModel.find();
  }

  findOne(id: string) {
    return this.filmeModel.findById(id)
  }

  update(id: string, updateFilmeDto: UpdateFilmeDto) {
    return this.filmeModel.findByIdAndUpdate({
      _id: id,
    },{
      $set: updateFilmeDto,
    },
    {
      new: true,
    });
  }

  remove(id: string) {
    return this.filmeModel.deleteOne({
      _id:id,
    })
    .exec();
  }
}
