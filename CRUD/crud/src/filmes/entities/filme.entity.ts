import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type FilmeDocument = Filme & Document;

@Schema()
export class Filme {
  @Prop()
  id: string;

  @Prop()
  nome: string;

  @Prop() 
  data: string;

  @Prop()
  autor: string;
}

export const FilmeSchema = SchemaFactory.createForClass(Filme);